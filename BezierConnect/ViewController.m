//
//  ViewController.m
//  BezierConnect
//
//  Created by Daniel.Burke on 4/21/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _square1 = [[UIView alloc] initWithFrame:CGRectMake(60, 360, 60, 60)];
    _square1.backgroundColor = [UIColor redColor];
    [self.view addSubview:_square1];
    
    _square2 = [[UIView alloc] initWithFrame:CGRectMake(200, 74, 60, 60)];
    _square2.backgroundColor = [UIColor blueColor];
    [self.view addSubview:_square2];
    
    _startPoint = CGPointMake(_square1.frame.origin.x + _square1.frame.size.width,
                          _square1.frame.origin.y + _square1.frame.size.height/2);
    _ctrPoint1 = CGPointMake(_square1.frame.origin.x + _square1.frame.size.width*2,
                          _square1.frame.origin.y + _square1.frame.size.height/2);
    _ctrPoint2 = CGPointMake(_square2.frame.origin.x - _square2.frame.size.width,
                          _square2.frame.origin.y + _square2.frame.size.height/2);
    _endPoint = CGPointMake(_square2.frame.origin.x,
                          _square2.frame.origin.y + _square2.frame.size.height/2);
    // init points here
    _connectionPath = (ConnectionPath*)[ConnectionPath bezierPath];
    [_connectionPath moveToPoint:_startPoint];
    [_connectionPath addCurveToPoint:_endPoint controlPoint1:_ctrPoint1 controlPoint2:_ctrPoint2];

    _connectionLayer = [CAShapeLayer layer];
    _connectionLayer.path = _connectionPath.CGPath;
    _connectionLayer.fillColor = [UIColor clearColor].CGColor;
    _connectionLayer.strokeColor = [UIColor redColor].CGColor;
    _connectionLayer.lineWidth = 2.0;
//    shapeLayer.position = CGPointMake(0, 0); //Use this to adjust position of this line
    [self.view.layer addSublayer:_connectionLayer];
    
}

-(void)animateCurve{
    
    //Animate point to point
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.3;
    pathAnimation.fromValue = @(0.0f);
    pathAnimation.toValue = @(1.0f);
    [_connectionLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
    
    NSLog(@"Do animation");
    _startPoint = CGPointMake(20 + _square1.frame.size.width,
                              _square1.frame.origin.y + _square1.frame.size.height/2);
    _ctrPoint1 = CGPointMake(20 + _square1.frame.size.width*2,
                             _square1.frame.origin.y + _square1.frame.size.height/2);
    _ctrPoint2 = CGPointMake(80 - _square2.frame.size.width,
                             _square2.frame.origin.y + _square2.frame.size.height/2);
    _endPoint = CGPointMake(80,
                            _square2.frame.origin.y + _square2.frame.size.height/2);
    // init points here
    UIBezierPath *newConnectionPath = (ConnectionPath*)[ConnectionPath bezierPath];
    [newConnectionPath moveToPoint:_startPoint];
    [newConnectionPath addCurveToPoint:_endPoint controlPoint1:_ctrPoint1 controlPoint2:_ctrPoint2];
    
    
    CGPathRef oldPath = _connectionLayer.path;
    CGPathRef newPath = newConnectionPath.CGPath;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    [animation setFromValue:(__bridge id)oldPath];
    [animation setToValue:(__bridge id)newPath];
    [animation setDuration:0.5];
    [animation setBeginTime:CACurrentMediaTime() + 0.3];
    [animation setFillMode:kCAFillModeBackwards];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_connectionLayer addAnimation:animation forKey:@"path"];
    _connectionLayer.path = newPath;
    
    
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _square1.frame = CGRectMake(20, 360, 60, 60);
        _square2.frame = CGRectMake(80, 74, 60, 60);
    } completion:^(BOOL finished) {
        
        _startPoint = CGPointMake(20 + _square1.frame.size.width,
                                  _square1.frame.origin.y + _square1.frame.size.height/2);
        _ctrPoint1 = CGPointMake(20 + _square1.frame.size.width*2,
                                 _square1.frame.origin.y + _square1.frame.size.height/2);
        _ctrPoint2 = CGPointMake(20 - _square2.frame.size.width,
                                 _square2.frame.origin.y + _square2.frame.size.height/2);
        _endPoint = CGPointMake(20,
                                _square2.frame.origin.y + _square2.frame.size.height/2);
        // init points here
        UIBezierPath *newConnectionPath = (ConnectionPath*)[ConnectionPath bezierPath];
        [newConnectionPath moveToPoint:_startPoint];
        [newConnectionPath addCurveToPoint:_endPoint controlPoint1:_ctrPoint1 controlPoint2:_ctrPoint2];
        
        CGPathRef oldPath = _connectionLayer.path;
        CGPathRef newPath = newConnectionPath.CGPath;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
        [animation setFromValue:(__bridge id)oldPath];
        [animation setToValue:(__bridge id)newPath];
        [animation setDuration:0.3];
        [animation setBeginTime:CACurrentMediaTime() + 0];
        [animation setFillMode:kCAFillModeBackwards];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [_connectionLayer addAnimation:animation forKey:@"path"];
        _connectionLayer.path = newPath;
        [UIView animateWithDuration:0.3 animations:^{
            _square2.frame = CGRectMake(20, 74, 60, 60);
        }];
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [self animateCurve];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    UIView *view = [touch view];
    if(view == _square1 || view == _square2){
        NSLog(@"Touch Position: %f, %f", [touch locationInView:self.view].x, [touch locationInView:self.view].y);
        view.center = [touch locationInView:self.view];
        
        _startPoint = CGPointMake(_square1.frame.origin.x + _square1.frame.size.width,
                                  _square1.frame.origin.y + _square1.frame.size.height/2);
        _ctrPoint1 = CGPointMake(_square1.frame.origin.x + _square1.frame.size.width*2,
                                 _square1.frame.origin.y + _square1.frame.size.height/2);
        _ctrPoint2 = CGPointMake(_square2.frame.origin.x - _square2.frame.size.width,
                                 _square2.frame.origin.y + _square2.frame.size.height/2);
        _endPoint = CGPointMake(_square2.frame.origin.x,
                                _square2.frame.origin.y + _square2.frame.size.height/2);
        // init points here
        UIBezierPath *newConnectionPath = (ConnectionPath*)[ConnectionPath bezierPath];
        [newConnectionPath moveToPoint:_startPoint];
        [newConnectionPath addCurveToPoint:_endPoint controlPoint1:_ctrPoint1 controlPoint2:_ctrPoint2];
        _connectionLayer.path = newConnectionPath.CGPath;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
