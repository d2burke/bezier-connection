//
//  ViewController.h
//  BezierConnect
//
//  Created by Daniel.Burke on 4/21/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionPath.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) UIView *square1;
@property (strong, nonatomic) UIView *square2;
@property (strong, nonatomic) ConnectionPath *connectionPath;
@property (strong, nonatomic) UIBezierPath *path;
@property (strong, nonatomic) CAShapeLayer *connectionLayer;

@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGPoint ctrPoint1;
@property (nonatomic) CGPoint ctrPoint2;
@property (nonatomic) CGPoint endPoint;

@end
