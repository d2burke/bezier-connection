//
//  ConnectionPath.m
//  BezierConnect
//
//  Created by Daniel.Burke on 4/21/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "ConnectionPath.h"

@implementation ConnectionPath

- (void)drawRect:(CGRect)rect
{
    UIColor *fillColor = [UIColor redColor];
    [fillColor setFill];
    UIColor *strokeColor = [UIColor blueColor];
    [strokeColor setStroke];
    [self closePath];
    [self fill];
    [self stroke];
}

@end
